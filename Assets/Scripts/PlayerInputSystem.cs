using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Profiling;
using Unity.Transforms;
using UnityEngine;
using Random = Unity.Mathematics.Random;

[UpdateInGroup(typeof(PrePhysicsGroup))]
public class PlayerInputSystem : SystemBase
{
    private GameSystem _game;
    private GameInputSystem _input;
    private BuildPhysicsWorld _buildPhysicsWorld;
    private const int ProjectileExpiryTicks = 60;

    protected override void OnCreate() {
        _game = World.GetOrCreateSystem<GameSystem>();
        _input = World.GetOrCreateSystem<GameInputSystem>();
        _buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
    }
    
    private struct IgnoreSelfCollector : ICollector<ColliderCastHit> {
        private readonly int _selfRigidBodyIndex;
        private readonly float3 _selfPosition;
        public ColliderCastHit closestHit;
        public float closestDistancesq;

        public bool EarlyOutOnFirstHit => false;
        public float MaxFraction => 1f;
        public int NumHits { get; private set; }

        public IgnoreSelfCollector(int selfRigidBodyIndex, float3 selfPosition) {
            NumHits = 0;
            _selfRigidBodyIndex = selfRigidBodyIndex;
            _selfPosition = selfPosition;
            closestHit = default;
            closestDistancesq = float.MaxValue;
        }

        public bool AddHit(ColliderCastHit hit) {
            if (hit.RigidBodyIndex == _selfRigidBodyIndex) {
                return false;
            }

            float distancesq = math.distancesq(hit.Position, _selfPosition);
            if (distancesq < closestDistancesq) {
                closestHit = hit;
                closestDistancesq = distancesq;
            }

            NumHits = 1;
            return true;
        }
    }

    struct ProjectileCreation {
        public float3 translation;
        public float3 impulse;
    }

    
    static readonly ProfilerMarker EachEntityMarker = new ProfilerMarker("PlayerInputSystem.EachEntity");
    static readonly ProfilerMarker EachInputMarker = new ProfilerMarker("PlayerInputSystem.EachInput");
    protected override void OnUpdate() {
        NativeList<PlayerInput> inputs = new NativeList<PlayerInput>(Allocator.Temp);
        _input.UnqueueInputs(inputs);
        
        var prefabList = GetSingleton<NetcodePrefabList>().prefabs;
        var projectilePrefab = EntityManager.GetBuffer<NetcodePrefabBuffer>(prefabList)[(int)AllNetcodePrefabs.Projectile].value;
        var projectileMass = EntityManager.GetComponentData<PhysicsMass>(projectilePrefab);
        
        NativeList<ProjectileCreation> projectilesToCreate = new NativeList<ProjectileCreation>(Allocator.Temp);
        
        Entities
            .WithoutBurst() // for _buildPhysicsWorld
            .ForEach((Entity entity,
                      ref PhysicsVelocity velocity,
                      ref PlayerInputData input,
                      ref PhysicsCollider collider,
                      in Translation translation,
                      in PhysicsMass mass,
                      in NetcodeObject netObj) => {
                using var autoscope = EachEntityMarker.Auto();
                bool shooting = false;
                float2 shootTarget = float2.zero;

                foreach (var playerInput in inputs) {
                    using var autoscopeInput = EachInputMarker.Auto();
                    if (playerInput.playerId != netObj.playerId) {
                        continue;
                    }

                    switch (playerInput.command) {
                        case InputCommand.shoot: {
                            shooting = true;
                            shootTarget = new float2(playerInput.payload.x, playerInput.payload.y);
                            break;
                        }
                    }
                }

                if(shooting)
                {
                    float3 launcherDirection = new float3(shootTarget.x, shootTarget.y, 0f);

                    float3 impulseDirection = translation.Value - launcherDirection;
                    impulseDirection = math.normalize(impulseDirection);

                    velocity.ApplyLinearImpulse(mass, impulseDirection * input.recoilImpulse);
                    var rand = Random.CreateFromIndex((uint)ServerTickStatic.serverTick);
                    var offset = new float3 {
                        x = rand.NextFloat() * 0.2f,
                        y = rand.NextFloat() * 0.2f,
                        z = rand.NextFloat() * 0.2f,
                    };
                    projectilesToCreate.Add(new ProjectileCreation {
                        impulse = (impulseDirection + offset) * input.recoilImpulse * -2f,
                        translation = translation.Value - impulseDirection * 0.5f,
                    });
                }
            }).Run();

        if (_game.IsServer()) {
            foreach (var p in projectilesToCreate) {
                var projectileVelocity = new PhysicsVelocity();
                projectileVelocity.ApplyLinearImpulse(projectileMass, p.impulse);
            
                var projectile = _game.InstantiateObject(new NetcodeObjectInitializer {
                    id = _game.GetNextNetcodeObjectId(),
                    position = p.translation,
                    rotation = quaternion.identity,
                    linearVelocity = projectileVelocity.Linear,
                    angularVelocity = projectileVelocity.Angular,
                    isInactive = false,
                    playerId = _input.playerId,
                    expiresOnTick = ServerTickStatic.serverTick + 60,
                    prefabIndex = (int) AllNetcodePrefabs.Projectile,
                    isDeathPrefab = false
                });
                EntityManager.SetComponentData(projectile, projectileVelocity);
            }
        }
    }
}
