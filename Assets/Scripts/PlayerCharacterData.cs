﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;

[MaterialProperty("_Color", MaterialPropertyFormat.Float4)]
public struct PlayerColor : IComponentData {
  public float4 color;
}
