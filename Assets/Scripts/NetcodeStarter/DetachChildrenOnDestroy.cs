﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct DetachChildrenOnDestroy : IComponentData {}
