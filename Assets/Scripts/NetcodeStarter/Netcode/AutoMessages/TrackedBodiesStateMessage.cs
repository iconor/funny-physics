﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateMessageSerialization]
public struct TrackedBodiesStateMessage : INetcodeMessage<TrackedBodiesStateMessage> {
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int serverTick;

  public NativeArray<TrackedBodyState> trackedBodies;

  [FieldQuantization("TrackedBodyState.PositionMin", "TrackedBodyState.PositionMax",
                     "TrackedBodyState.PositionBits")]
  public float3 basePosition;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }

    world.GetExistingSystem<GameSystem>().PurgeTempEntities();
    world.GetExistingSystem<GameSystem>()
         .ProcessTrackedBodiesStateMessage(this, ref skipDispose);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return false;
  }

  public void Dispose() {
    trackedBodies.Dispose();
  }
}

[GenerateMessageFieldSerialization]
public struct TrackedBodyState {
  public static readonly int IdBits = 16;
  public static readonly int IdMin = 0;
  public static readonly int IdMax = 65536;
  public static readonly int3 PositionBits = new int3(32, 32, 32);
  public static readonly double3 PositionMin = new double3(-20 * 255, -50 * 255, -20 * 255);
  public static readonly double3 PositionMax = new double3(20 * 255, 200 * 255, 20 * 255);
  public static readonly int3 RelPositionBits = new int3(20, 20, 8);
  public static readonly double3 RelPositionMin = new double3(-25, -50, -10);
  public static readonly double3 RelPositionMax = new double3(25, 150, 10);
  public static readonly int3 VelocityBits = new int3(16, 16, 8);
  public static readonly double3 VelocityMin = new double3(-100, -100, -10);
  public static readonly double3 VelocityMax = new double3(100, 100, 10);
  public static readonly int3 AngularVelocityBits = new int3(8, 8, 16);
  public static readonly double3 AngularVelocityMin = new double3(-50, -50, -50);
  public static readonly double3 AngularVelocityMax = new double3(50, 50, 50);

  private static readonly int TotalQuantizedBytes =
    (IdBits +
     RelPositionBits.x + RelPositionBits.y + RelPositionBits.z +
     VelocityBits.x + VelocityBits.y + VelocityBits.z + 1 +
     AngularVelocityBits.x + AngularVelocityBits.y + AngularVelocityBits.z + 1 +
     Quantization.QuaternionBits + 1) / 8 + 1;

  private static readonly NativeArray<byte> TmpQuantizationBytes =
    new NativeArray<byte>(TotalQuantizedBytes, Allocator.Persistent);

  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int id;

  [FieldQuantization("TrackedBodyState.RelPositionMin", "TrackedBodyState.RelPositionMax",
                     "TrackedBodyState.RelPositionBits")]
  public float3 position;

  [FieldQuantization("TrackedBodyState.VelocityMin", "TrackedBodyState.VelocityMax",
                     "TrackedBodyState.VelocityBits")]
  public float3 velocity;

  [FieldQuantization("TrackedBodyState.AngularVelocityMin", "TrackedBodyState.AngularVelocityMax",
                     "TrackedBodyState.AngularVelocityBits")]
  public float3 angularVelocity;

  public quaternion rotation;

  public TrackedBodyState ApplyQuantization() {
    var bitOffset = 0;
    if (!NetcodeMessageSerialization.Serialize(this, TmpQuantizationBytes, ref bitOffset, 0xfffffff)
    ) {
      Debug.LogError("Failed to quantize into tmp bytes");
    }

    bitOffset = 0;
    if (!NetcodeMessageSerialization.Deserialize(TmpQuantizationBytes, ref bitOffset,
                                                 Allocator.None, 0xfffffff,
                                                 out TrackedBodyState result)) {
      Debug.LogError("Failed to quantize into tmp bytes");
    }

    return result;
  }
}
