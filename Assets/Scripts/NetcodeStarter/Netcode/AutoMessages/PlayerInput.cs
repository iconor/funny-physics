﻿using System;
using Unity.Mathematics;

[GenerateMessageFieldSerialization]
public struct PlayerInput : IDisposable {
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int serverTick;
  public InputCommand command;
  [FieldQuantization("QuantizationConstants.InputPayloadMin", "QuantizationConstants.InputPayloadMax", "QuantizationConstants.InputPayloadBits")]
  public float3 payload;
  public void Dispose() { }
}
