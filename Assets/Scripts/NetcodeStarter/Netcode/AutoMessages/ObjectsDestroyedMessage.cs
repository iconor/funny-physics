﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct ObjectsDestroyedMessage : INetcodeMessage<ObjectsDestroyedMessage> {
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public NativeArray<int> objectIds;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }

    world.GetExistingSystem<GameSystem>().DestroyObjects(objectIds);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return true;
  }

  public void Dispose() {
    objectIds.Dispose();
  }
}
