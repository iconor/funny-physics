﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct PlayerEnteredMessage : INetcodeMessage<PlayerEnteredMessage> {
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  
  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (isServer) {
      return false;
    }
    world.GetOrCreateSystem<GameSystem>().AddPlayerDataEntity(playerId);
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return true;
  }

  public void Dispose() { }
}