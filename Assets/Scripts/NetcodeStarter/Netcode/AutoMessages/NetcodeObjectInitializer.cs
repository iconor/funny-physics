﻿using System;
using Unity.Collections;
using Unity.Mathematics;

[GenerateMessageFieldSerialization]
public struct NetcodeObjectInitializer : IDisposable {
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int id;
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int playerId;
  [FieldQuantization(QuantizationConstants.MinPrefabIndex, QuantizationConstants.MaxPrefabIndex)]
  public int prefabIndex;
  [FieldQuantization("TrackedBodyState.PositionMin", "TrackedBodyState.PositionMax", "TrackedBodyState.PositionBits")]
  public float3 position;
  public quaternion rotation;
  [FieldQuantization("TrackedBodyState.VelocityMin", "TrackedBodyState.VelocityMax", "TrackedBodyState.VelocityBits")]
  public float3 linearVelocity;
  [FieldQuantization("TrackedBodyState.AngularVelocityMin", "TrackedBodyState.AngularVelocityMax", "TrackedBodyState.AngularVelocityBits")]
  public float3 angularVelocity;
  [FieldQuantization(QuantizationConstants.MinServerTick, QuantizationConstants.MaxServerTick)]
  public int expiresOnTick;
  public bool isDeathPrefab;
  public bool isInactive;
  public void Dispose() {}
}
