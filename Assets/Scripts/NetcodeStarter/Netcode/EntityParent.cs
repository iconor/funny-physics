﻿using Unity.Entities;

public struct EntityParent : IComponentData {
  public Entity value;
}
