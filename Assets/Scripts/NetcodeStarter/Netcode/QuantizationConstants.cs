﻿using Unity.Mathematics;

public static class QuantizationConstants {
  public const long MinPlayerId = -256;
  public const long MaxPlayerId = 256;
  public const long MinNetObjId = 0;
  public const long MaxNetObjId = 1 << 20;
  public const long MinPrefabIndex = 0;
  public const long MaxPrefabIndex = 1 << 9;
  public const long MinServerTick = -256;
  public const long MaxServerTick = (long)int.MaxValue + 1;
  public static readonly int3 InputPayloadBits = new int3(12, 12, 8);
  public static readonly double3 InputPayloadMin = new double3(-100f, -100f, 1f);
  public static readonly double3 InputPayloadMax = new double3(100f, 100f, 1f);
}
