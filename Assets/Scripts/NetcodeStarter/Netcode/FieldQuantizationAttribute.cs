﻿using System;
using System.Globalization;
using Unity.Mathematics;
using UnityEngine;

public class FieldQuantizationAttribute : Attribute {
  public string minStr;
  public string maxStr;
  public string bitsStr;

  public FieldQuantizationAttribute(long min, long max) {
    minStr = min.ToString();
    maxStr = max.ToString();
    bitsStr = Quantization.BitsForValue(max - min).ToString();
  }

  public FieldQuantizationAttribute(float min, float max, int bits) {
    minStr = min.ToString(CultureInfo.InvariantCulture);
    maxStr = max.ToString(CultureInfo.InvariantCulture);
    bitsStr = bits.ToString();
  }

  public FieldQuantizationAttribute(string minStr, string maxStr, string bitsStr) {
    this.minStr = minStr;
    this.maxStr = maxStr;
    this.bitsStr = bitsStr;
  }
}
