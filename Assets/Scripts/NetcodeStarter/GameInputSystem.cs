﻿using System.Collections.Generic;
using System.Threading;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

[UpdateInGroup(typeof(PrePhysicsGroup))]
[AlwaysUpdateSystem]
public class GameInputSystem : ComponentSystem {
  public int playerId;

  private readonly Dictionary<int, int> _unitsToPlayers =
    new Dictionary<int, int>();

  private int _turnIndex;
  private int _latestPlayerId;
  private GameSystem _game;

  private readonly Dictionary<int, UnitMicroInputMessage> _unitMicroInputs =
    new Dictionary<int, UnitMicroInputMessage>();

  private readonly ServerTickedMessageQueue<PlayerInput> _playerInputQueue =
    new ServerTickedMessageQueue<PlayerInput>(ServerTickSelectionCriteria.AtOrPastTick);

  protected override void OnCreate() {
    InitEntityQueryCache(30);
    _game = World.GetOrCreateSystem<GameSystem>();
  }

  public int GetPlayerForUnit(int unitId) {
    if (_unitsToPlayers.TryGetValue(unitId, out int playerId)) {
      return playerId;
    }

    Debug.LogError($"Player does not exist for unit {unitId}");
    return int.MinValue;
  }
  
  
  public void QueueInput(PlayerInput playerInput) {
    if (playerInput.playerId == playerId) {
      _game.QueueForDirectOrIndirectBroadcast(new PlayerInputMessage {
        playerInput = playerInput
      });
    }

    _playerInputQueue.QueueMessage(playerInput.serverTick, playerInput);
  }

  public void UnqueueInputs(NativeList<PlayerInput> inputs) {
    while (_playerInputQueue.GetNextMessage(out var input)) {
      inputs.Add(input);
    }
  }

  public int GetNewPlayerId() {
    if (!_game.IsServer()) {
      Debug.LogError("Client requesting a new player id.");
      return -1;
    }

    return Interlocked.Increment(ref _latestPlayerId);
  }

  public void RegisterUnit(int unitId, int playerId) {
    _unitsToPlayers[unitId] = playerId;
  }

  public void SetMicroInputState(UnitMicroInputMessage message) {
    _unitMicroInputs[message.unitId] = message;
  }

  protected override void OnUpdate() {
  }
}

public enum InputCommand {
  move,
  shoot,
  jump,
  jumpHold
}
