﻿using Unity.Entities;
using Unity.Physics.Systems;

[UpdateBefore(typeof(BuildPhysicsWorld))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class PrePhysicsGroup : ComponentSystemGroup {}

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
public class PostPhysicsGroup : ComponentSystemGroup {}
