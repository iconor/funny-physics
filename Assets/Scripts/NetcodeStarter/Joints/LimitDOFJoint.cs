﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Authoring;
using UnityEngine;

public class LimitDOFJoint : BaseJoint
{
    public bool3 LockLinearAxes;
    public bool3 LockAngularAxes;

    public static PhysicsJoint CreateLimitDOFJoint(RigidTransform offset, bool3 linearLocks, bool3 angularLocks)
    {
        var constraints = new FixedList128<Constraint>();
        if (math.any(linearLocks))
        {
            constraints.Add(new Constraint
            {
                ConstrainedAxes = linearLocks,
                Type = ConstraintType.Linear,
                Min = 0,
                Max = 0,
                SpringFrequency = Constraint.DefaultSpringFrequency,
                SpringDamping = Constraint.DefaultSpringDamping
            });
        }
        if (math.any(angularLocks))
        {
            constraints.Add(new Constraint
            {
                ConstrainedAxes = angularLocks,
                Type = ConstraintType.Angular,
                Min = 0,
                Max = 0,
                SpringFrequency = Constraint.DefaultSpringFrequency,
                SpringDamping = Constraint.DefaultSpringDamping
            });
        }

        var joint = new PhysicsJoint
        {
            BodyAFromJoint = BodyFrame.Identity,
            BodyBFromJoint = offset
        };
        joint.SetConstraints(constraints);
        return joint;
    }

    public override void Create(EntityManager entityManager, GameObjectConversionSystem conversionSystem)
    {
        if (!math.any(LockLinearAxes) && !math.any(LockAngularAxes))
            return;

        RigidTransform bFromA = math.mul(math.inverse(worldFromB), worldFromA);
        conversionSystem.World.GetOrCreateSystem<EndJointConversionSystem>().CreateJointEntity(
            this,
            GetConstrainedBodyPair(conversionSystem),
            CreateLimitDOFJoint(bFromA, LockLinearAxes, LockAngularAxes)
        );
    }
}

public abstract class BaseJoint : BaseBodyPairConnector
{
    public bool EnableCollision;

    void OnEnable()
    {
        // included so tick box appears in Editor
    }

    protected PhysicsConstrainedBodyPair GetConstrainedBodyPair(GameObjectConversionSystem conversionSystem)
    {
        return new PhysicsConstrainedBodyPair(
            conversionSystem.GetPrimaryEntity(this),
            ConnectedBody == null ? Entity.Null : conversionSystem.GetPrimaryEntity(ConnectedBody),
            EnableCollision
        );
    }
}

[RequireComponent(typeof(PhysicsBodyAuthoring))]
public abstract class BaseBodyPairConnector : MonoBehaviour
{
    public PhysicsBodyAuthoring LocalBody => GetComponent<PhysicsBodyAuthoring>();
    public PhysicsBodyAuthoring ConnectedBody;

    public RigidTransform worldFromA => LocalBody == null
        ? RigidTransform.identity
        : Math.DecomposeRigidBodyTransform(LocalBody.transform.localToWorldMatrix);

    public RigidTransform worldFromB => ConnectedBody == null
        ? RigidTransform.identity
        : Math.DecomposeRigidBodyTransform(ConnectedBody.transform.localToWorldMatrix);


    public Entity EntityA { get; set; }

    public Entity EntityB { get; set; }


    void OnEnable()
    {
        // included so tick box appears in Editor
    }

    public abstract void Create(EntityManager entityManager, GameObjectConversionSystem conversionSystem);
}