﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

[UpdateInGroup(typeof(PrePhysicsGroup))]
public class PlayerRespawnSystem : SystemBase {
  protected override void OnUpdate() {
    if (TryGetSingleton(out BattlefieldBounds bounds)) {
      Entities.ForEach((ref Translation translation, ref PhysicsVelocity velocity, in PlayerStartLocation startLocation) => {
        var x = translation.Value.x;
        var y = translation.Value.y;
        if (x < bounds.minX || x > bounds.maxX || y < bounds.minY || y > bounds.maxY) {
          translation.Value = new float3(startLocation.value.x, startLocation.value.y, 0);
          velocity = new PhysicsVelocity();
        }
      }).Run();
    }
  }
}
