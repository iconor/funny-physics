﻿using Unity.Entities;

[UpdateInGroup(typeof(PostPhysicsGroup))]
public class EntityExpirySystem : SystemBase {
  EndSimulationEntityCommandBufferSystem _endSimulationEcb;
  protected override void OnCreate() {
    base.OnCreate();
    _endSimulationEcb = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
  }

  protected override void OnUpdate() {
    var serverTick = ServerTickStatic.serverTick;
    var ecb = _endSimulationEcb.CreateCommandBuffer();
    Entities.ForEach((Entity entity, in ExpiresOnTick exp) => {
      if (serverTick >= exp.tick) {
        ecb.DestroyEntity(entity);
      }
    }).Run();
  }
}

public struct ExpiresOnTick : IComponentData {
  public int tick;
}
