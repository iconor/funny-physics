﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct BattlefieldBounds : IComponentData {
  public float minX;
  public float minY;
  public float maxX;
  public float maxY;
}
