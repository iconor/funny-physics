using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
[Serializable]
public struct PlayerInputData : IComponentData
{
    public float baseSpeed;
    public float accel;
    public float jumpStartVelocity;
    public float jumpHoldForce;
    public float recoilImpulse;
    public int jumpGracePeriod;
    public int jumpCooldown;
    public int jumpHoldEnd;

    public int lastJumpInput;

    public KeyCode leftKey;
    public KeyCode rightKey;
    public KeyCode jumpKey;
    public int lastJumpExecute;
}
