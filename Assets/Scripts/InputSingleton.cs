﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

public class InputSingleton : MonoBehaviour {
    private GameSystem _game;
    private GameInputSystem _input;

    private void Start() {
        _game = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<GameSystem>();
        _input = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<GameInputSystem>();
    }

    private void Update() {
        float moveDirection = 0f;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
            moveDirection -= 1f;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
            moveDirection += 1f;
        }

        var inputTick = _game.GetJitterBufferedServerTick();
        if (moveDirection != 0f) {
            var moveInput = new PlayerInput {
                command = InputCommand.move,
                payload = new float3(moveDirection, 0f, 0f),
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(moveInput);
        }

        bool jump = Input.GetKeyDown(KeyCode.Space);
        if (jump) {
            var jumpInput = new PlayerInput {
                command = InputCommand.jump,
                payload = float3.zero,
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(jumpInput);
        } else if (Input.GetKey(KeyCode.Space)) {
            var jumpHoldInput = new PlayerInput {
                command = InputCommand.jumpHold,
                payload = float3.zero,
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(jumpHoldInput);
        }

        bool shoot = Input.GetMouseButton(0);
        if (shoot) {
            float3 mousePosition = Input.mousePosition;
            mousePosition.z = Camera.main.nearClipPlane;
            float3 aimLocation = Camera.main.ScreenToWorldPoint(mousePosition);
            aimLocation.z = 0.0f;
            var shootInput = new PlayerInput {
                command = InputCommand.shoot,
                payload = new float3(aimLocation.x, aimLocation.y, 0f),
                playerId = _input.playerId,
                serverTick = inputTick,
            };
            _input.QueueInput(shootInput);
        }
    }
}
